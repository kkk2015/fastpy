port=$1
ps -ef | grep proxy | grep $port | awk '{print $2}' | xargs -i{} kill -9 {}
nohup python proxy.py $port > /dev/null 2>&1 &

